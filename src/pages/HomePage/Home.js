import React from 'react'
import { InfoSection } from '../../components';
import {homeObjOne,homeObjTwo, homeObjThree} from './Data';
function Home() {
    return (
        <div>
            <>
            <InfoSection {...homeObjOne} />
            <InfoSection {...homeObjTwo} />
            <InfoSection {...homeObjThree} />
            </>
        </div>
    )
}

export default Home;
