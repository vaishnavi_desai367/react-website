import React from 'react';
import { ContactSection } from '../../components';

import {contactSectionObj} from './Data';

function Contacts() {
    return(
        <div>
        <ContactSection {...contactSectionObj}/>
        </div>
    );
};


export default Contacts;