import React from "react";
import { Navbar } from "./components";
import {BrowserRouter, Switch, Route, Redirect} from 'react-router-dom';
import Home from './pages/HomePage/Home';
import Contacts from './pages/Contacts/Contacts';

// import { Button, Alert } from "reactstrap";

// import { v4 as uuidv4 } from 'uuid'; /// uuid import exemple //

//if you want use axios -->>> import_npm_list.txt inside axios you can learn from web page//

function App() {
  return (
    <BrowserRouter>
    <Navbar />
    <Switch>
       <Route path='/' exact component={Home} />
       <Route path='/contact' exact component={Contacts} />
    </Switch>
    <Redirect to="/"></Redirect>
    </BrowserRouter>
  );
}

export default App;
