import React from "react";

import { ElementWrapper, InfoHeadline, InfoContainer, InfoText,ImageWrapper } from "./InfoSection.elements";

const InfoSection = ({ description, headline,img }) => {
  return (
    <InfoContainer>
        <ElementWrapper>
           <InfoHeadline>{headline}</InfoHeadline>
           <InfoText>{description}</InfoText>
        </ElementWrapper>
      <ImageWrapper>
          <img src={img}></img>
      </ImageWrapper>
    </InfoContainer>
  );
};

export default InfoSection;
