import styled from 'styled-components';

export const InfoWrapper = styled.div`
padding:160px 0;
color:#fff;
background-color:red;
height:100% ;
width: 100%;
`;

export const InfoContainer = styled.div`
background-color : #05012b;
display:flex;
flex-direction:row;
padding:3rem;
`;

export const ElementWrapper = styled.div`
padding:3rem;
`;
export const InfoText = styled.p`
color:#fff;

`;
export const InfoHeadline = styled.h2`
color:#fff;
`;
export const ImageWrapper = styled.div`
display:flex;
max-width:550px;
justify-content:flex-end;
img {
    max-width:100% !important;
}
`
