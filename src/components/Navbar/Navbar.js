import React,{ useState, useEffect } from 'react';
import { Nav, NavIconContainer, NavIcon, MobileIcon, NavMenu, NavItem, Navlink,NavButtonElement, ButtonLink } from './Navbar.elements';
import {FaTimes,FaBars} from 'react-icons/fa';
import { IconContext } from 'react-icons/lib';
import { NavLink } from 'react-router-dom';

const Navbar = () => {
    const [click,setClick] = useState(false);
    const clickHendler = () => setClick(!click);
    const [button,setButton] = useState(true);
    const showButton = () => {
        if (window.innerWidth <= 960){
            setButton(false);
        }else {
            setButton(true);
        }
    }
    useEffect(()=> {
        showButton();
    },[]);
    window.addEventListener('resize',showButton);
    return (
    <>
    <IconContext.Provider value = {{color:'#fff'}}>
        <Nav>
            <NavIconContainer to="/">
                <NavIcon />
                  <span>BRAND</span>
            </NavIconContainer>
            <MobileIcon onClick={clickHendler}>
                {click ? <FaTimes/> : <FaBars/>}
            </MobileIcon>
            <NavMenu onClick={clickHendler} click={click}>
                <NavItem>
                    <Navlink to='/'>Home</Navlink>
                </NavItem>
                <NavItem>
                    <Navlink exact to='/contact'>Contact</Navlink>
                </NavItem>
                <NavItem>
                    <Navlink to='/about'>About</Navlink>
                </NavItem>
                <NavButtonElement>
                    {
                    button ? (<ButtonLink to='/sing-up'>SING UP</ButtonLink>) : (<ButtonLink to='/sing-up'>SING UP</ButtonLink>)
                    }
                </NavButtonElement>
            </NavMenu>
        </Nav>
    </IconContext.Provider>
        </>
    );
}

export default Navbar;