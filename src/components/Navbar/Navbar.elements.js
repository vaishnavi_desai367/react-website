import styled from "styled-components"
import { Link } from 'react-router-dom';
import { FaMagento } from 'react-icons/fa';


export const Nav = styled.nav`
display:flex;
justify-content:space-around;
background-color:#05012b;
width: 100%;
height:80px;
`;

export const NavIconContainer = styled(Link)`
display:flex;
justify-self:flex-start;
align-items:center;
cursor:pointer;
text-decoration:none;
font-weight:700;
color:white;
font-size:2rem;
&:hover{
    text-decoration:none;
    color:#fff;
}
`;

export const NavIcon = styled(FaMagento)`
margin-right:.5rem;
`;

export const MobileIcon = styled.div `
display:none;
font-weight:700;
font-size:2rem;
@media screen and (max-width : 960px){
    display: block;
}
`;

export const NavMenu = styled.ul`
display:flex;
align-items:center;
align-self: flex-end;
text-align:center;
list-style:none;
@media screen and (max-width:960px){
display:flex;
flex-direction:column;
width: 100%;
height: 90vh;
position:absolute;
top:80px;
background-color : #05012b;
transition : all .5s ease;
left:${({click}) => (click ? 0 : '-100%')};
}
`;

export const NavItem = styled.li`
border-bottom:2px solid transparent;
font-weight:700;
color:#fff;
margin:0 1rem;
&:hover {
    border-bottom:2px solid #ffffff;
    cursor: pointer;
}
@media screen and (max-width:960px){
width: 100%;
padding:2rem;
&:hover {
    border-bottom:2px solid transparent;
}
}
`;

export const Navlink = styled(Link)`
display:flex;
align-items:center;
text-decoration:none;
color:#fff;
&:hover{
    color:#fff;
    text-decoration:none;
}
`;

export const NavButtonElement =styled.li `
background-color:#1e0fa1;
padding:5px;
padding:.7rem;
border-radius:.7rem;
@media screen and (max-width:960px){
width: 50% !important;
margin:0 auto;
display: block;
}
`;

export const ButtonLink =styled(Link) `
color:#fff;
text-decoration:none;
&:hover {
color:#fff;
text-decoration:none;
}
`;
