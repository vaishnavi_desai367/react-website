import React from 'react';
import {ContactWrapper,ContactText} from './ContactSection.elements';

const ContactSection = ({text}) => {
    return(
        <>
        <ContactWrapper>
            <ContactText>
                {text}
            </ContactText>
        </ContactWrapper>
        </>
    );
};


export default ContactSection;